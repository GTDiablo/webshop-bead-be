const jwt = require('jsonwebtoken');

function extractUserData(req, res, next) {
  // Check for authorization header
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    return res.status(401).json({ message: 'Missing authorization header' });
  }

  // Extract token from header
  const token = authHeader.split(' ')[1];

  try {
    // Verify token and extract user data
    const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
    const { userId, username, isProvider } = decodedToken;

    // Add user data to request object
    req.userId = userId;
    req.username = username;
    req.isProvider = isProvider;

    // Call next middleware
    next();
  } catch (error) {
    res.status(401).json({ message: 'Invalid or expired token' });
  }
}

module.exports = extractUserData;
