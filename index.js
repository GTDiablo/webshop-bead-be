const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();

const bodyParser = require('body-parser');
const db = require('./db');
const User = require('./models/users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const extractUserData = require('./token.middleware');
const {NeedOfferSchema, Need} = require("./models/need");
const app = express();

// Middleware
app.use(morgan('tiny'));
app.use(cors());
app.use(bodyParser.json());

// Registration
app.post('/api/register', async (req, res) => {
  const { username, password, isProvider } = req.body;

  // Check if username already exists
  const existingUser = await User.findOne({ username });
  if (existingUser) {
    return res.status(409).json({ message: 'Username already exists' });
  }

  // Hash the password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  // Create the new user
  const user = new User({
    username,
    isProvider,
    password: hashedPassword,
  });
  await user.save();

  res.json({ message: 'User created successfully' });
});

// Login
app.post('/api/login', async (req, res) => {
  const { username, password } = req.body;

  // Check if user exists
  const user = await User.findOne({ username });
  if (!user) {
    return res.status(401).json({ message: 'Invalid username or password' });
  }

  // Check if password is correct
  const isPasswordValid = await bcrypt.compare(password, user.password);
  if (!isPasswordValid) {
    return res.status(401).json({ message: 'Invalid username or password' });
  }

  // Create a JWT token
  const token = jwt.sign({ userId: user._id, username: user.username, isProvider: user.isProvider }, process.env.JWT_SECRET);

  res.json({ token });
});

app.post('/api/needs', extractUserData, async (req, res) => {
  try {
    const { userId, isProvider } = req;

    if(isProvider){
      return res.json({'Message': 'Csak vevőként tudsz'});
    }

    const { items } = req.body;
    const todo = new Need({
      author: userId,
      items
    });
    await todo.save();
    res.json(todo);
  } catch (error) {
    res.status(401).json({ message: 'Invalid or expired token' });
  }
});

// Read all todos
app.get('/api/needs', async (req, res) => {
  const todos = await Need.find().populate('author').populate('offers.author');
  res.json(todos);
});

// Read a single todo
app.get('/api/needs/:id', async (req, res) => {
  const { id } = req.params;
  const todo = await Need.findById(id).populate('author').populate('offers.author');
  res.json(todo);
});

// Make an offer
app.post('/api/needs/:id/offers', extractUserData, async (req, res) => {
  const {id} = req.params;
  const {isProvider, userId } = req;

  if(!isProvider){
    return res.json({'message': 'Csak beszerzők!'});
  }

  const {price} = req.body;
  const newOffer = {
    author: userId,
    price: price,
  }
  const need = await Need.findByIdAndUpdate(id, {
    $push: {
      offers: newOffer,
    },
  });
  res.json(need);
});

// Accept Offer
app.post('/api/needs/:id/offers/:offerId/accept', extractUserData, async (req, res) => {
  const {id, offerId} = req.params;
  const {isProvider, userId} = req;

  if(isProvider){
    return res.json({'message': 'Vevők'});
  }

  const need = await Need.findOneAndUpdate({
    _id: id,
    offers: {
      $elemMatch: {
        '_id': offerId
      }
    }
  }, {
    $set:{
      isOpen: false,
      'offers.$.isAccepted': true
    }
  }, {new: true});

  if(need.author.toString() !== userId){
    return res.json({message: 'Csak a tulaj tudja elfogadni'});
  }


  res.json(need);

});

app.get('/api/me', extractUserData, async (req, res) => {
  const { userId } = req;

  const myNeeds =  await Need.find({author: userId, isOpen: true}).populate('author').populate('offers.author');
  const myCompletedNeeds =  await Need.find({author: userId, isOpen: false}).populate('author').populate('offers.author');
  const myOffers = await Need.find({'offers.author': userId, 'offers.isAccepted': false}).populate('author').populate('offers.author');
  const myAcceptedOffers = await Need.find({'offers.author': userId, 'offers.isAccepted': true}).populate('author').populate('offers.author');

  res.json({
    myNeeds,
    myCompletedNeeds,
    myOffers,
    myAcceptedOffers
  })
});



// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
