const mongoose = require('mongoose');

const NeedOfferSchema = new mongoose.Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  isAccepted: {
    type: Boolean,
    default: false,
  },
  price: {
    type: Number,
    required: true
  }
});

const NeedItemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  amount: {
    type: Number,
    required: true,
  }
});

const needSchema = new mongoose.Schema({
  isOpen: {
    type: Boolean,
    default: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  items: [NeedItemSchema],
  offers: [NeedOfferSchema]
}, {timestamps: true});

const Need = mongoose.model('Need', needSchema);

module.exports = {
  Need,
  NeedItemSchema,
  NeedOfferSchema
};